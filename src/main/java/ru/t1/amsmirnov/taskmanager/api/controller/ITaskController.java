package ru.t1.amsmirnov.taskmanager.api.controller;

public interface ITaskController {

    void createTask();

    void showTasks();

    void showTaskById();

    void showTaskByIndex();

    void showTaskByProjectId();

    void updateTaskById();

    void updateTaskByIndex();

    void changeTaskStatusById();

    void changeTaskStatusByIndex();

    void startTaskById();

    void startTaskByIndex();

    void completeTaskById();

    void completeTaskByIndex();

    void clearTasks();

    void removeTasksById();

    void removeTasksByIndex();

}