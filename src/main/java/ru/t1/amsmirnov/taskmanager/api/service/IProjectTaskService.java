package ru.t1.amsmirnov.taskmanager.api.service;

public interface IProjectTaskService {

    void bindTaskToProject(String projectId, String taskId);

    void removeProjectById(String projectId);

    void unbindTaskFromProject(String projectId, String taskId);

}
