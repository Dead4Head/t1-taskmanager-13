package ru.t1.amsmirnov.taskmanager.api.service;

import ru.t1.amsmirnov.taskmanager.api.repository.ICommandRepository;

public interface ICommandService extends ICommandRepository {
}
