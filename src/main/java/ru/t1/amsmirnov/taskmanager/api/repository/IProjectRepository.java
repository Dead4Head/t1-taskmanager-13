package ru.t1.amsmirnov.taskmanager.api.repository;

import ru.t1.amsmirnov.taskmanager.model.Project;

import java.util.List;

public interface IProjectRepository {

    void add(Project project);

    List<Project> findAll();

    void clear();

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    void remove(Project project);

    Project removeById(String id);

    Project removeByIndex(Integer index);

    Integer getSize();

    Boolean existById(String projectId);

}
